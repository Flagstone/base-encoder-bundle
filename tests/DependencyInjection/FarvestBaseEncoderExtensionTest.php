<?php


namespace Farvest\BaseEncoderBundle\Tests\DependencyInjection;

use Farvest\BaseEncoderBundle\DependencyInjection\FarvestBaseEncoderExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Exception;

class FarvestBaseEncoderExtensionTest extends TestCase
{
    /**
     *  @var FarvestBaseEncoderExtension
     */
    private $extension;
    private $root;

    public function setUp(): void
    {
        $this->extension = new FarvestBaseEncoderExtension();
        $this->root = "farvest_base_encoder";
    }

    /**
     *  @throws Exception
     */
    public function testLoadParameters()
    {
        $this->extension->load(array(), $container = $this->getContainer());

        $this->assertTrue($container->hasDefinition('fv_encoder.base3'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base4'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base8'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base16'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base32'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base32_hex'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base64'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base181'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base362'));
        $this->assertTrue($container->hasDefinition('fv_encoder.base529'));

        $this->assertTrue($container->hasAlias('Farvest\\BaseEncoderBundle\\Encoder\\BaseEncoderInterface'));

    }

    private function getContainer()
    {
        $container = new ContainerBuilder();
        return $container;
    }
}