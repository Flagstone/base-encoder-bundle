<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;


use Farvest\BaseEncoderBundle\Encoder\Base32Encoder;
use PHPUnit\Framework\TestCase;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

class BaseEncoderExceptionsTest extends TestCase
{

    /** *************************************************************************************************************
     *  Test that new string for the base has the good length.
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function testBaseLengthErrorException()
    {
        $encoder = new Base32Encoder();

        $this->expectException(BaseLengthErrorException::class);
        $encoder->changeBaseString('abcdefghijklmnopqrstuvwxyz');
        $encoder->encode('small');
    }

    /** *************************************************************************************************************
     *  Test that new string must contains only unique chars.
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function testNonUniqueCharactersBaseStringException()
    {
        $encoder = new Base32Encoder();

        $this->expectException(NonUniqueCharactersBaseStringException::class);
        $this->expectExceptionMessage(sprintf('The base string contain non unique characters ([%s] found at least 2 times)', 'f'));
        $encoder->changeBaseString('abcdeffghijklmnopqrstuvwxyz01234');
    }

    /** *************************************************************************************************************
     *  Test that base string uses to decode the string is compatible with the string
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function testInvalidEncodedStringException()
    {
        $encoder = new Base32Encoder();

        $this->expectException(InvalidEncodedStringException::class);
        $encoder->encode('small');
        $encoder->changeBaseString('abcdefghijklmnopqrstuvwxyz012345');
        $encoder->decode('ONWWC3DM');
    }
}
