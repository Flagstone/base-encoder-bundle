<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base64Encoder;
use PHPUnit\Framework\TestCase;

class Base64EncoderTest extends TestCase
{
    public function testEncoder()
    {
        $encoder = new Base64Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function testDecoder()
    {
        $encoder = new Base64Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $decodedString = $encoder->decode($testValues[1]);
            $this->assertEquals($testValues[0], $decodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', 'c21hbGw='],
            ['medium string to encode', 'bWVkaXVtIHN0cmluZyB0byBlbmNvZGU='],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', 'TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZWFzb24sIGJ1dCBieSB0aGlzIHNpbmd1bGFyIHBhc3Npb24gZnJvbSBvdGhlciBhbmltYWxzLCB3aGljaCBpcyBhIGx1c3Qgb2YgdGhlIG1pbmQsIHRoYXQgYnkgYSBwZXJzZXZlcmFuY2Ugb2YgZGVsaWdodCBpbiB0aGUgY29udGludWVkIGFuZCBpbmRlZmF0aWdhYmxlIGdlbmVyYXRpb24gb2Yga25vd2xlZGdlLCBleGNlZWRzIHRoZSBzaG9ydCB2ZWhlbWVuY2Ugb2YgYW55IGNhcm5hbCBwbGVhc3VyZS4=']
        ];
    }
}
