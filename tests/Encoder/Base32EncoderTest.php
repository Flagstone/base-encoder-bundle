<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base32Encoder;
use PHPUnit\Framework\TestCase;

class Base32EncoderTest extends TestCase
{
    public function testEncoder()
    {
        $encoder = new Base32Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function testDecoder()
    {
        $encoder = new Base32Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $decodedString = $encoder->decode($testValues[1]);
            $this->assertEquals($testValues[0], $decodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', 'ONWWC3DM'],
            ['medium string to encode', 'NVSWI2LVNUQHG5DSNFXGOIDUN4QGK3TDN5SGK==='],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', 'JVQW4IDJOMQGI2LTORUW4Z3VNFZWQZLEFQQG433UEBXW43DZEBRHSIDINFZSA4TFMFZW63RMEBRHK5BAMJ4SA5DINFZSA43JNZTXK3DBOIQHAYLTONUW63RAMZZG63JAN52GQZLSEBQW42LNMFWHGLBAO5UGSY3IEBUXGIDBEBWHK43UEBXWMIDUNBSSA3LJNZSCYIDUNBQXIIDCPEQGCIDQMVZHGZLWMVZGC3TDMUQG6ZRAMRSWY2LHNB2CA2LOEB2GQZJAMNXW45DJNZ2WKZBAMFXGIIDJNZSGKZTBORUWOYLCNRSSAZ3FNZSXEYLUNFXW4IDPMYQGW3TPO5WGKZDHMUWCAZLYMNSWKZDTEB2GQZJAONUG64TUEB3GK2DFNVSW4Y3FEBXWMIDBNZ4SAY3BOJXGC3BAOBWGKYLTOVZGKLQ=']
        ];
    }
}
