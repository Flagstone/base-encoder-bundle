<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base16Encoder;
use PHPUnit\Framework\TestCase;

class Base16EncoderTest extends TestCase
{
    public function testEncoder()
    {
        $encoder = new Base16Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function testDecoder()
    {
        $encoder = new Base16Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $decodedString = $encoder->decode($testValues[1]);
            $this->assertEquals($testValues[0], $decodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', '736D616C6C'],
            ['medium string to encode', '6D656469756D20737472696E6720746F20656E636F6465'],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', '4D616E2069732064697374696E677569736865642C206E6F74206F6E6C792062792068697320726561736F6E2C2062757420627920746869732073696E67756C61722070617373696F6E2066726F6D206F7468657220616E696D616C732C2077686963682069732061206C757374206F6620746865206D696E642C20746861742062792061207065727365766572616E6365206F662064656C6967687420696E2074686520636F6E74696E75656420616E6420696E6465666174696761626C652067656E65726174696F6E206F66206B6E6F776C656467652C2065786365656473207468652073686F727420766568656D656E6365206F6620616E79206361726E616C20706C6561737572652E']
        ];
    }
}
