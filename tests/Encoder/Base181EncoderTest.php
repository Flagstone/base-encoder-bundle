<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base181Encoder;
use PHPUnit\Framework\TestCase;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

class Base181EncoderTest extends TestCase
{
    public function testEncoder()
    {
        $encoder = new Base181Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function testDecoder()
    {
        $encoder = new Base181Encoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $decodedString = $encoder->decode($testValues[1]);
            $this->assertEquals($testValues[0], $decodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', 'fffeidfjbdim='],
            ['medium string to encode', 'fbgciefeeenhhjbncibfgmcjijdadafgfciacfbjhblhlagkdkdg=='],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', 'dihgihhkbddaejekdebfhdinhkdcjjblemmbifjheccbgcdekkljhhcbilmikbjhbhcbclhkbdajccecaejfcabhjifakdmibhcbckenafeadjjiaekhdbndjhhkkkkffcbfijnbbbmkdcgdadnjhdijhkdekdlkekmjciffdfbdlblagdkdgkjlgdeijljlehikcjihafeclalehabfdagfhkdmdaebbhcgckemkeemajbeeiadhiijgmnkkcmjfcbebafkbfabahnfdbbjammmgaicdafcekdhcjjhefbkiblibanfbdkegakcjhfgejhmignmecjhajgkaeedfdldjkmijgbceinbihjdbdcnjinieaejabnlijcidaejfcbecfbhhbhagickfhbjamjdgmnkjjajfcbeiinceeldcbnmkhihfkgegajikedffhnmcfbhbcmehkbeaebnjbflgnjijfclbhckcgbddfbefbdalhfjhhcbkcbejkenfbgcihimkbmiajbeeiadahgjkjdcjdldfekdcdlfafecailmeadbhdjdjfjaeebc=']
        ];
    }
}
