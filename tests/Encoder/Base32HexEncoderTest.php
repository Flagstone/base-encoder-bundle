<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base32HexEncoder;
use PHPUnit\Framework\TestCase;

class Base32HexEncoderTest extends TestCase
{
    public function testEncoder()
    {
        $encoder = new Base32hexEncoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function testDecoder()
    {
        $encoder = new Base32HexEncoder();

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $decodedString = $encoder->decode($testValues[1]);
            $this->assertEquals($testValues[0], $decodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', 'EDMM2R3C'],
            ['medium string to encode', 'DLIM8QBLDKG76T3ID5N6E83KDSG6ARJ3DTI6A==='],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', '9LGMS839ECG68QBJEHKMSPRLD5PMGPB45GG6SRRK41NMSR3P41H7I838D5PI0SJ5C5PMURHC41H7AT10C9SI0T38D5PI0SR9DPJNAR31E8G70OBJEDKMURH0CPP6UR90DTQ6GPBI41GMSQBDC5M76B10ETK6IOR841KN683141M7ASRK41NMC83KD1II0RB9DPI2O83KD1GN8832F4G6283GCLP76PBMCLP62RJ3CKG6UPH0CHIMOQB7D1Q20QBE41Q6GP90CDNMST39DPQMAP10C5N68839DPI6APJ1EHKMEOB2DHII0PR5DPIN4OBKD5NMS83FCOG6MRJFETM6AP37CKM20PBOCDIMAP3J41Q6GP90EDK6USJK41R6AQ35DLIMSOR541NMC831DPSI0OR1E9N62R10E1M6AOBJELP6ABG=']
        ];
    }
}
