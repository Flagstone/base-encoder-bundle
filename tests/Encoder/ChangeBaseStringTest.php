<?php


namespace Farvest\BaseEncoderBundle\Tests\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Base32Encoder;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use PHPUnit\Framework\TestCase;

class ChangeBaseStringTest extends TestCase
{
    /** *************************************************************************************************************
     *  Test that changeBaseString method return good encoded strings
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function testChangeBaseStringEncoding()
    {
        $encoder = new Base32Encoder();

        $encoder->changeBaseString('abcdefghijklmnopqrstuvwxyz234567');

        $testSuite = $this->encoderProvider();

        foreach ($testSuite as $testValues) {
            $encodedString = $encoder->encode($testValues[0]);
            $this->assertEquals($testValues[1], $encodedString);
        }
    }

    public function encoderProvider(): array
    {
        return [
            ['small', 'onwwc3dm'],
            ['medium string to encode', 'nvswi2lvnuqhg5dsnfxgoidun4qgk3tdn5sgk==='],
            ['Man is distinguished, not only by his reason, but by this singular passion from other animals, which is a lust of the mind, that by a perseverance of delight in the continued and indefatigable generation of knowledge, exceeds the short vehemence of any carnal pleasure.', 'jvqw4idjomqgi2ltoruw4z3vnfzwqzlefqqg433uebxw43dzebrhsidinfzsa4tfmfzw63rmebrhk5bamj4sa5dinfzsa43jnztxk3dboiqhayltonuw63ramzzg63jan52gqzlsebqw42lnmfwhglbao5ugsy3iebuxgidbebwhk43uebxwmidunbssa3ljnzscyidunbqxiidcpeqgcidqmvzhgzlwmvzgc3tdmuqg6zramrswy2lhnb2ca2loeb2gqzjamnxw45djnz2wkzbamfxgiidjnzsgkztboruwoylcnrssaz3fnzsxeylunfxw4idpmyqgw3tpo5wgkzdhmuwcazlymnswkzdteb2gqzjaonug64tueb3gk2dfnvsw4y3febxwmidbnz4say3bojxgc3baobwgkyltovzgklq=']
        ];
    }
}
