<?php
/** *****************************************************************************************************************
 *  Base91Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/12/02
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

/** *****************************************************************************************************************
 *  Class Base64Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base91 format.
 *  basE91 has been developed by Joachim Henke, and is released as free software under the terms of the BSD license.
 *  Initial algorithm has been changed to use string base instead of array base.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base91Encoder extends AbstractBaseEncoder
{
    const SPLIT = 13;                                                                                           //  How many bit per char
    const POWER = 1;                                                                                            //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!#$%&()*+,./:;<=>?@[]^_`{|}~"'; //  Characters to use to obtain the coded string
    const BASE_LENGTH = 91;                                                                                     //  Base length
    const BYTES_BLOCK_LENGTH = 0;                                                                               //  Length of each block

    /** *************************************************************************************************************
     *  Base91Encoder constructor.
     *  @throws Exceptions\NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function __construct()
    {
        $this->changeBaseString(self::BASE);
    }

    public function encode(?string $string = ""): string
    {
        $stringLength = strlen($string);
        $returnString = '';
        $b = 0;
        $n = 0;
        for ($iter = 0; $iter < $stringLength; ++$iter) {
            $b |= ord($string[$iter]) << $n;
            $n += 8;
            if ($n > 13) {
                $v = $b & 8191;
                if ($v > 88) {
                    $b >>= 13;
                    $n -= 13;
                } else {
                    $v = $b & 16383;
                    $b >>= 14;
                    $n -= 14;
                }
                $returnString .= substr(self::BASE, $v % 91, 1) . substr(self::BASE, floor(($v / 91)), 1);
            }
        }
        if ($n) {
            $returnString .= substr(self::BASE, $b % 91, 1);
            if ($n > 7 || $b > 90) {
                $returnString .= substr(self::BASE, floor($b / 91), 1);
            }
        }
        return $returnString;
    }

    public function decode(?string $string = ""): string
    {
        $stringLength = strlen($string);
        $v = -1;
        $b = 0;
        $n = 0;
        $returnString = '';

        for ($iter = 0; $iter < $stringLength; ++$iter) {
            $c = strpos(self::BASE, $string[$iter]);
            if (false === $c) {
                continue;
            }
            if ($v < 0) {
                $v = $c;
            } else {
                $v += $c * 91;
                $b |= $v << $n;
                $n += ($v & 8191) > 88 ? 13 : 14;
                do {
                    $returnString .= chr($b & 255);
                    $b >>= 8;
                    $n -= 8;
                } while ($n > 7);
                $v = -1;
            }
        }
        if ($v + 1) {
            $returnString .= chr(($b | $v << $n) & 255);
        }
        return $returnString;
    }
}