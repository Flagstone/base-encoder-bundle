<?php
/** *****************************************************************************************************************
 *  Base32Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/25
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;

/** *****************************************************************************************************************
 *  Class Base32Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base32 format.
 *  Respect RFC 4648 (https://tools.ietf.org/html/rfc4648#section-5) with Extended Hex Alphabet
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base32HexEncoder extends Base32Encoder
{
    const BASE = '0123456789ABCDEFGHIJKLMNOPQRSTUV';    //  Characters to use to obtain the coded string

    /** *************************************************************************************************************
     *  Base32HexEncoder constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function __construct()
    {
        parent::__construct();
        $this->changeBaseString(self::BASE);
    }
}
