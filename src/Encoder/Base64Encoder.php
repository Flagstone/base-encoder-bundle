<?php
/** *****************************************************************************************************************
 *  Base64Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/27
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

/** *****************************************************************************************************************
 *  Class Base64Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base64 format.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base64Encoder extends AbstractBaseEncoder
{
    const SPLIT = 6;                                                                    //  How many bit per char
    const POWER = 1;                                                                    //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';    //  Characters to use to obtain the coded string
    const BASE_LENGTH = 64;                                                             //  Base length
    const BYTES_BLOCK_LENGTH = 4;                                                       //  Length of each block

    use BaseEncoderTrait;
}