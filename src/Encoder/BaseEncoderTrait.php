<?php
/** *****************************************************************************************************************
 *  BaseEncoderTrait.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/28
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;

/** *****************************************************************************************************************
 *  Class BaseEncoderTrait
 *  -----------------------------------------------------------------------------------------------------------------
 *  Contains __construct, encode and decode method to avoid repetitive same methods in all encoders class
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
trait BaseEncoderTrait
{
    /** *************************************************************************************************************
     *  Base32Encoder constructor.
     *  ************************************************************************************************************* */
    public function __construct()
    {
        $this->changeBaseString(self::BASE);
    }

    /** *************************************************************************************************************
     *  Encode the string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string|null $string
     *  @return string
     *  ************************************************************************************************************* */
    public function encode(?string $string = ""): string
    {
        $this->testBaseLength($this->baseString, strlen(self::BASE));
        return $this->encodeString($string, self::SPLIT, self::POWER, $this->baseString, self::BASE_LENGTH, self::NB_CHAR_PER_SPLIT, self::BYTES_BLOCK_LENGTH);
    }

    /** *************************************************************************************************************
     *  Decode the string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string|null $string
     *  @return string
     *  ************************************************************************************************************* */
    public function decode(?string $string = ""): string
    {
        return $this->decodeString($string, self::SPLIT, self::POWER, $this->baseString, self::BASE_LENGTH, self::NB_CHAR_PER_SPLIT);
    }
}