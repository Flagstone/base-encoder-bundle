<?php
/** *****************************************************************************************************************
 *  Base8Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/22
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

/** *****************************************************************************************************************
 *  Class Base8Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base181 format.
 *  A 15 bits length string is coded in a 32 bits string (4 chars). Use 14 different chars for encoding.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base181Encoder extends AbstractBaseEncoder
{
    const SPLIT = 15;               //  How many bit per char
    const POWER = 2;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abcdefghijklmn';  //  Characters to use to obtain the coded string
    const BASE_LENGTH = 181;        //  Base length
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}
