<?php
/** *****************************************************************************************************************
 *  Base4Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/22
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

/** *****************************************************************************************************************
 *  Class Base4Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base4 format.
 *  A 4 bits length string is coded in a 8 bits string (1 char). Use 4 different chars for encoding.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base4Encoder extends AbstractBaseEncoder
{
    const SPLIT = 2;                //  How many bit per char
    const POWER = 1;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 1;
    const BASE = '0123';            //  Characters to use to obtain the coded string
    const BASE_LENGTH = 4;          //  Base length
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}