<?php
/** *****************************************************************************************************************
 *  Base3Encoder.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/22
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;

/** *****************************************************************************************************************
 *  Class Base3Encoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Code and decode a string in Base3 format.
 *  A 3 bits length string is coded in a 16 bits string (2 chars). Use 3 different chars for encoding.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
class Base3Encoder extends AbstractBaseEncoder
{
    const SPLIT = 3;                //  How many bit per char
    const POWER = 1;                //  How many time decoding/encoding
    const NB_CHAR_PER_SPLIT = 2;
    const BASE = 'abc';             //  Characters to use to obtain the coded string
    const BASE_LENGTH = 3;          //  Base length
    const BYTES_BLOCK_LENGTH = 0;   //  Length of each block

    use BaseEncoderTrait;
}