<?php
/** ******************************************************************************************************************
 *  AbstractBaseEncoder.php
 *  ******************************************************************************************************************
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  *****************************************************************************************************************
 *  Created: 2019/11/22
 ******************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\Encoder;

use Farvest\BaseEncoderBundle\Encoder\Exceptions\BaseLengthErrorException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\InvalidEncodedStringException;
use Farvest\BaseEncoderBundle\Encoder\Exceptions\NonUniqueCharactersBaseStringException;

/** *****************************************************************************************************************
 *  Abstract Class AbstractBaseEncoder
 *  -----------------------------------------------------------------------------------------------------------------
 *  Class that manage encoder method.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle\Encoder
 *  ***************************************************************************************************************** */
abstract class AbstractBaseEncoder implements BaseEncoderInterface
{
    /** -------------------------------------------------------------------------------------------------------------
     *  @var string
     *  ------------------------------------------------------------------------------------------------------------- */
    protected $baseString;

    /** *************************************************************************************************************
     *  Transform the string in input in a binary string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @return string
     *  ************************************************************************************************************* */
    private function inBinaryTransform(string $string): string
    {
        $binary = '';

        for ($iter = 0; $iter < strlen($string); $iter++) {
            $byte = ord(substr($string, $iter, 1));
            $binary .= sprintf('%08d', decbin($byte));
        }

        return $binary;
    }

    /** *************************************************************************************************************
     *  Transform a binary string to an ASCII string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $binary
     *  @return string
     *  ************************************************************************************************************* */
    private function inStringTransform(string $binary): string
    {
        $string = '';

        for ($iter = 0; $iter < strlen($binary); $iter += 8) {
            $byte = substr($binary, $iter, 8);
            if (strlen($byte) === 8) {
                $string .= chr(bindec($byte));
            }
        }

        return $string;
    }

    /** *************************************************************************************************************
     *  Encode a string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @param int $split
     *  @param int $power
     *  @param string $base
     *  @param int $length
     *  @param int $nbChar
     *  @param int $bytesBlockLength
     *  @return string
     *  ************************************************************************************************************* */
    protected function encodeString(string $string, int $split, int $power, string $base, int $length, int $nbChar, int $bytesBlockLength = 0): string
    {
        $binary = $this->inBinaryTransform($string);
        $iter = 0;
        $finalString = '';
        $format = '%0'.$split.'d';
        $zeroToAdd = 0;

        while ($iter < strlen($binary)) {
            if ($iter + $split > strlen($binary)) {
                $zeroToAdd = $split - (strlen($binary) - $iter);
                $stringToEncode = substr($binary, $iter, strlen($binary) - $iter) . str_repeat('0', $zeroToAdd);
                $iter =  strlen($binary);
            } else {
                $stringToEncode = substr($binary, $iter, $split) ;
                $iter += $split;
            }
            $stringValue = bindec(sprintf($format, $stringToEncode));

            if ($power === 2) {
                $length = strlen($base);
                $firstGroup = floor($stringValue / pow($length, 2));
                $secondGroup = $stringValue % pow($length, 2);
                $finalString .= $this->split($firstGroup, $base).$this->split($secondGroup, $base);
            } else {
                if ($nbChar === 1) {
                    $finalString .= substr($base, $stringValue, $nbChar);
                } elseif ($nbChar === 2) {
                    $finalString .= $this->split($stringValue, $base);
                }
            }
        }

        /** ---------------------------------------------------------------------------------------------------------
         *  Add '=' padding characters at the end of the encoded string to signify that extra 0 had been added at the
         *  end of the binary string string. Each 8 '0' block add one '=' char.
         *  --------------------------------------------------------------------------------------------------------- */
        if ($zeroToAdd > 0 && $bytesBlockLength === 0) {
            $finalString .= str_repeat('=', floor(($zeroToAdd - 1) / 8) + 1);
        }

        /** ---------------------------------------------------------------------------------------------------------
         *  For Base64Encoder and Base32Encoder RFC compatibility
         *  --------------------------------------------------------------------------------------------------------- */
        if ($bytesBlockLength > 0 && strlen($finalString) % $bytesBlockLength !== 0) {
            $finalString .= str_repeat('=', $bytesBlockLength - strlen($finalString) % $bytesBlockLength);
        }

        return $finalString;
    }

    /** *************************************************************************************************************
     *  Decode a string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @param int $split
     *  @param int $power
     *  @param string $base
     *  @param int $length
     *  @param int $nbChar
     *  @return string
     *  @throws InvalidEncodedStringException
     *  ************************************************************************************************************* */
    protected function decodeString(string $string, int $split, int $power, string $base, int $length, int $nbChar): string
    {
        $this->testValidEncodedString($string);

        if ($power !== 2) {
            if ($nbChar === 1) {
                $format = '%0'.$split.'d';
                return $this->decodeUniqueChar($string, $format, $base);
            }
            return $this->decodeNonUniqueChar($string, $base, $nbChar);
        }

        $binary = $this->decodeWithPower($string, $base, $split);

        /** ---------------------------------------------------------------------------------------------------------
         *  Added '=' characters added at the end of the encoded string management
         *  --------------------------------------------------------------------------------------------------------- */
        $addedEquals = substr_count($string, '=');
        if ($addedEquals > 0) {
            $binaryLength = floor((strlen($binary) - $addedEquals - (($addedEquals - 1) * $split)) / 8) * 8 + 8;
            $binary = substr($binary, 0, $binaryLength);
            if ('00000000' === substr($binary, -8)) {
                $binary = substr($binary, 0, strlen($binary) - 8);
            }
        }

        return $this->inStringTransform($binary);
    }

    /** *************************************************************************************************************
     *  Decode string for unique char encoding (NB_CHAR_PER_SPLIT to 1)
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @param string $format
     *  @param string $base
     *  @return string
     *  ************************************************************************************************************* */
    private function decodeUniqueChar(string $string, string $format, string $base): string
    {
        $binary = '';
        for ($iter = 0; $iter < strlen($string); $iter++) {
            if ('=' !== substr($string, $iter, 1)) {
                $char = substr($string, $iter, 1);
                $key = strpos($base, $char);
                $binary .= sprintf($format, decbin($key));
            }
        }
        return $this->inStringTransform($binary);
    }

    /** *************************************************************************************************************
     *  Decode string for unique char encoding (NB_CHAR_PER_SPLIT > 1)
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @param string $base
     *  @param int $nbChar
     *  @return string
     *  ************************************************************************************************************* */
    private function decodeNonUniqueChar(string $string, string $base, int $nbChar): string
    {
        $binary = '';
        for ($iter = 0; $iter < strlen($string); $iter += $nbChar) {
            $subString = substr($string, $iter, $nbChar);
            if ('=' !== substr($subString, 0, 1)) {
                $chars = [];
                for ($iter2 = 0; $iter2 < $nbChar; $iter2 ++) {
                    $chars[] = substr($subString, $iter2, 1);
                }
                $binary .= $this->assemble($chars, $base);
            }
        }

        return $this->inStringTransform($binary);
    }

    /** *************************************************************************************************************
     *  Decode string for double or more encoding (POWER > 1)
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $string
     *  @param string $base
     *  @param int $split
     *  @return string
     *  ************************************************************************************************************* */
    private function decodeWithPower(string $string, string $base, int $split): string
    {
        $binary = '';

        for ($iter = 0; $iter < strlen($string); $iter += 4) {
            $pair1 = substr($string, $iter, 2);
            $pair2 = substr($string, $iter + 2, 2);

            if (false === strpos($pair1, '=')) {
                $pos1pair1 = substr($pair1, 0, 1);
                $pos2pair1 = substr($pair1, 1, 1);

                $pos1pair2 = substr($pair2, 0, 1);
                $pos2pair2 = substr($pair2, 1, 1);

                $binary .= $this->assemble([$pos1pair1, $pos2pair1, $pos1pair2, $pos2pair2], $base, $split);
            }
        }

        return $binary;
    }

    /** *************************************************************************************************************
     *  Test if the string is compatible with the base used to decode the string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $encoded
     *  @throws InvalidEncodedStringException
     *  ************************************************************************************************************* */
    public function testValidEncodedString(string $encoded)
    {
        for ($iter = 0; $iter < strlen($encoded); $iter++) {
            $char = $encoded[$iter];
            if (false === strpos($this->baseString.'=', $char)) {
                throw new InvalidEncodedStringException('The encoded string is not compatible with the base used to decode the string. Incorrect characters detected.');
            }
        }
    }

    /** *************************************************************************************************************
     *  Change the string of the base.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $newString
     *  @throws NonUniqueCharactersBaseStringException
     *  ************************************************************************************************************* */
    public function changeBaseString(string $newString)
    {
        $chars = [];
        for ($iter = 0; $iter < strlen($newString); $iter++) {
            $char = substr($newString, $iter, 1);
            if ('=' !== $char && in_array($char, $chars)) {
                throw new NonUniqueCharactersBaseStringException(sprintf('The base string contain non unique characters ([%s] found at least 2 times)', $char));
            } else {
                $chars[] = $char;
            }
        }

        $this->baseString = $newString;
    }

    /** *************************************************************************************************************
     *  Test if the string length of the base is equal to the string length expected
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $base
     *  @param int $baseLength
     *  @throws BaseLengthErrorException
     *  ************************************************************************************************************* */
    protected function testBaseLength(string $base, int $baseLength): void
    {
        if (strlen($base) !== $baseLength) {
            throw new BaseLengthErrorException(sprintf(
                'The length of the base string isn\'t correct (%s digit expected, %s characters in base)',
                $baseLength,
                strlen($base)
            ));
        }
    }

    /** *************************************************************************************************************
     *  @param int $value
     *  @param string $base
     *  @return string
     *  ************************************************************************************************************* */
    private function split(int $value, string $base): string
    {
        $baseLength = strlen($base);
        $firstChar = floor($value / $baseLength);
        $secondChar = $value % $baseLength;

        return substr($base, $firstChar,1).substr($base, $secondChar, 1);
    }

    /** *************************************************************************************************************
     *  @param array $chars
     *  @param string $base
     *  @param int $length
     *  @return string
     *  ************************************************************************************************************* */
    private function assemble(array $chars, string $base, int $length = 0): string
    {
        $baseLength = strlen($base);
        if ($length === 0) {
            $format = '%0' . $baseLength . 'd';
        } else {
            $format = '%0' . $length . 'd';
        }

        $value = 0;

        for ($iter = 0; $iter < count($chars); $iter++) {
            $char = $chars[$iter];
            $pow = (count($chars) - 1) - $iter;
            $value += pow($baseLength, $pow) * strpos($base, $char);
        }

        return sprintf($format, decbin($value));
    }
}
