<?php
/** *****************************************************************************************************************
 *  FarvestBaseEncoderExtension.php
 *  -----------------------------------------------------------------------------------------------------------------
 *  @copyright 2019 Farvest
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange@farvest.com>
 *  -----------------------------------------------------------------------------------------------------------------
 *  Created: 2019/11/21
 *  ***************************************************************************************************************** */

namespace Farvest\BaseEncoderBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Exception;

/** *****************************************************************************************************************
 *  Class FarvestBaseEncoderExtension
 *  -----------------------------------------------------------------------------------------------------------------
 *  Base class of the Farvest Base Encoder Bundle project
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\BaseEncoderBundle
 *  ***************************************************************************************************************** */
class FarvestBaseEncoderExtension extends Extension
{
    /** **************************************************************************************************************
     *  Load bundle resources
     *  --------------------------------------------------------------------------------------------------------------
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}